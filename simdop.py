#!/usr/bin/python

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from simdop import default
from simdop.utils import generationTask, testTask, schedulerTask

import sys, getopt, os.path

def usage(short = False):
    usage = """
    -h --help                       Prints this
    -r <file> --rdl=<file>          RDL file
    -g <file> --graph=<file>        Graph output file
    -s <type> --scheduler=<type>    Run a scheduler
    -c <file> --config=<file>       Select a config file
    --generation                    Create a random workload"""

    dflt = """
Default value:
    Configuration file: """ + default.__fileConfig + """
    RDL file:           """ + default.__fileRDL + """
    Graph output file:  """ + default.__fileGraph

    schedType = """
Scheduler types:
    GREEDY
    GREEDY_L
    GREEDY_LB"""

    example = """

Examples
    Command to generate a valid RDL file:
        """ + os.path.basename(__file__) + """ -cconfig/workload.json""" \
        """ -r""" + default.__fileRDL + """ --generation

    Command to run GREEDY and GREEDY_LB schedulers
        """ + os.path.basename(__file__) + """ -c""" + default.__fileConfig + \
        """ -r""" + default.__fileRDL + """ -sGREEDY,GREEDY_LB"""

    usageShort = "Try '" + os.path.basename(__file__) + \
                 "' for more information."

    if short:
        print usageShort
    else:
        print usage
        print dflt
        print schedType
        print example
        print 


def main(argv):
    fileConfig = default.__fileConfig
    fileRDL = default.__fileRDL
    fileGraph = default.__fileGraph

    scheduler = default.__scheduler
    schedType = default.__schedType
    
    generation = default.__generation
    graph = default.__graph

    try:
        opts, args = getopt.getopt(argv, "hr:c:g:s:",
                ["help", "rdl=", "config=", "graph=", "generation", "scheduler="])
        for opt, arg in opts:
            if opt in ("-h", "--help"):
                usage()
                sys.exit(1)
            elif opt in ("-r", "--rdl"):
                if not arg == "":
                    fileRDL = arg
            elif opt in ("-c", "--config"):
                if not arg == "":
                    fileConfig = arg
            elif opt in ("-g", "--graph"):
                if not arg == "":
                    fileGraph = arg
                graph = True
            elif opt in ("--generation"):
                generation = True
            elif opt in ("-s", "--scheduler"):
                if not arg == "":
                    schedType = arg
                scheduler = True

    except getopt.GetoptError as err:
        print err.msg
        usage(True)
        sys.exit(2)

    if generation :
        generationTask(fileConfig, fileRDL, graph, fileGraph)
    elif scheduler :
        schedType = schedType.split(",")
        for sType in schedType:
            schedulerTask(fileConfig, sType, fileRDL, graph, fileGraph)
            print
    elif not generation and not scheduler:
        usage()

if __name__ == '__main__':
    main(sys.argv[1:])
