#!/bin/bash
DIR_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../"
TEST_PATH="${DIR_PATH}test/"
SAVE_WORKLOAD_PATH="${TEST_PATH}workloads/"

EXEC_NAMEFILE="${DIR_PATH}simdop.py"

SCHED=(GREEDY GREEDY_L GREEDY_LB)

gen="$EXEC_NAMEFILE -r"$TEST_PATH"workload.rdl -c"$TEST_PATH"workload.json --generation"
run="$EXEC_NAMEFILE -r"$TEST_PATH"workload.rdl -c"$TEST_PATH"config.json -s"

rm -f $TEST_PATH"workload.json"
rm -f $TEST_PATH"workload.rdl"
rm -f $TEST_PATH"data.dat"
rm -rf $SAVE_WORKLOAD_PATH

mkdir $SAVE_WORKLOAD_PATH

cat > $TEST_PATH"config.json" <<- EOC
{
    "cluster": {
        "bundle": 1000
    }
}
EOC

for j in `seq 50 50 2500`
do

    echo "JOB $j"

    min=`echo "scale=6; 1.5/($j*40)" | bc`
    max=`echo "scale=6; 1.5/($j*4)" | bc`

    ## Generataion of config file
    cat > $SAVE_WORKLOAD_PATH"workload$j.json" <<- EOC
{
    "test": {
        "bundle": 1000,
        "end": 100,
        "load": 1.5,
        "rigid_ratio": 0.2,
        "tasks_per_job": {"min": 6, "max": 10},
        "w": {"min": 0$min, "max": 0$max},
        "h": {"min": 0.01, "max": 0.3}
    }
}
EOC

    cp $SAVE_WORKLOAD_PATH"workload$j.json" $TEST_PATH"workload.json"

    ## Generation of workload from config file
    $gen > /dev/null 2>&1
    cp $TEST_PATH"workload.rdl" $SAVE_WORKLOAD_PATH"workload$j.rdl"
    count=`cat $TEST_PATH"workload.rdl" | grep 'job' | wc -l`
    echo "Job generated: $count"

    ## Running the schedulers and retrive info
    len=`expr ${#SCHED[@]} - 1`
    for i in `seq 0 $len`
    do
        echo -n ${SCHED[$i]}" "
        TMP=`$run ${SCHED[$i]} 2> /dev/null`
        s=`echo "$TMP" | grep "Scheduler" | cut -f2 -d' '`
        t=`echo "$TMP" | grep "Time" | cut -f2 -d' '`
        p=`echo "$TMP" | grep "Preemption" | cut -f3 -d' '`
        l=`echo "$TMP" | grep "Load" | cut -f2 -d' '`
        echo $count $s $t $p $l >> $TEST_PATH"data.dat"
    done
    echo
    echo
done

echo -n "Generating graphs"
Rscript $TEST_PATH"test.R" $TEST_PATH &> /dev/null
rm -f $DIR_PATH"Rplots.pdf"
echo 
