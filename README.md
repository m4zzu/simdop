# README #

**simdop.py** is a tool for the simulation of scheduling algorithms on big-data systems for the reservation of cluster resources. This work specifically targets HADOOP YARN and supports a subset of the RDL language presented in *Curino, Carlo, et al. "Reservation­‐based Scheduling: If You're Late Don't Blame Us!."*.
The tool is implemented in python and relies on R for the generation of some of the graphs. For a complete list of the dependencies required by the tool see the "DEPENDENCIES" section at the end of this document.

## RDL SUPPORT

Currently we support the usage of *Window*, *Atom*, *All* and *Order* operators (*Any* is not supported yet). The *Window* operator must be used at the top level of the expression while *Order* and *All* can be nested. 

Within the Atom operator the following parameters can be defined:

* **w**: the workload for the task;
* **h**: the maximum number of bundles that can be allocated for the task during a specific time instant;
* **rigid**: True if the task is rigid, False otherwise.

If a task is defined as rigid it must be executed for **w/h** continuous time instants with a fixed allocation of **h** resources. On the other hand, if the task is not rigid (malleable task), it only requires the execution of workload **w** with no constraints on fixed allocation.

The following is an example of the definition of a job using our RDL syntax:

```
#!python
job j1
	window 3 36
	task j1t0 369 202 False
	task j1t1 320 80 True
	task j1t2 204 121 False
	task j1t3 38 222 False
	task j1t4 239 194 False
	rdl A(O(j1t1,A(j1t0,j1t2)),O(j1t3,j1t4))
```

The definition of the job starts with the *job* keyword followed by the name of the job. The declaration is then divided in three main parts: (1) the definition of the window frame, (2) the definitions of the atoms (tasks) by means of a task label followed by the task id, **w**, **h** and **rigid** parameters, and (3) the RDL expression in which *A* stands for *All*, *O* stands for *Order* and atoms are identified by the task ids previously defined. Multiple jobs can be defined in the same file to generate a complex workload.

## GENERATING A WORKLOAD

simdop supports the generation of random workloads whose parameters can be specified by means of a json file. An example of a configuration file can be found in *config/workload.json*:


```
#!json
{
    "test": {
        "bundle": 100,
        "end": 100,
        "load": 1.0,
        "rigid_ratio": 0.2,
        "tasks_per_job": {"min": 2, "max": 12},
        "w": {"min": 0.0001, "max": 0.01},
        "h": {"min": 0.01, "max": 0.3}
    }
}
```

*bundle* specifies the number of bundles available within the system, *end* specify the length of the timeframe to consider, *load* the overall load to generate (0.7 means generating a workload whose schedule on the system could lead to an utilization of 70% for the considered timeframe), *rigid_ratio* is the ratio of rigid tasks to generate, *task_per_job* specifies a uniform distribution for the number of tasks to generate for each job, *w* specifies a uniform distribution for the generation of **w** parameter while *h* specifies a uniform distribution for the generation of **h** parameter.

In order to generate a workload file (workload.rdl) from the config file in *config/workload.json* the following command can be used:

```
#!bash
./simdop.py -r workload.rdl -c config/workload.json --generation
```

## IMPLEMENTED SCHEDULERS

Within the tool we implemented 3 algorithms: GREE, GREE-L and GREE-LB. Algorithms GREE and GREE-L are based on the work of Curino et al., while GREE-LB is a revised implementation of GREE-L including several optimizations.
The main features of the schedulers are listed in the following sections. All the implemented algorithms support nesting of *All* and *Order* operators within the RDL expressions.

### GREE scheduler
* Schedule all tasks ALAP;
* Tries to assign as much resources as possible to each task;
* Leads to high accepted load at the cost of high resource variations.
  
### GREE-L scheduler
* Exploits the entire window to reduce resource variations;
* Selects the interval in which each task should be placed trying to reduce overall resources variation;
* Leads to low resources variation but also reduced accepted load.

### GREE-LB scheduler
* Similarly to GREE-L exploits the entire available window to reduce resources variation;
* Enhance the acceptance rate by reshaping malleable tasks and by moving rigid tasks that cannot be planned;
* Leads on average to higher accepted load than GREE-L and lower resources variation than GREE;

### Running a scheduler

simdop allows to run one or more schedulers on a specific workload and retrieve metrics on the final results. The following command can be used to run GREE, GREE-L and GREE-LB on workload.rdl: 

```
#!bash
./simdop.py -r workload.rdl -c config/config.json -s GREEDY,GREEDY_L,GREEDY_LB
```

Notice that the extra config file *config/config.json* is required to specify the number of bundles available within the cluster. 

It is also possible to generate a graph representing the allocation of the different tasks on the cluster. The following command runs GREE-LB and tells simdop to generate a graph of the plan:

```
#!bash
./simdop.py -r workload.rdl -c config/config.json -s GREEDY_LB -g graph
```

## RUNNING TESTS ###

In order to compare the different implementations of the schedulers we have implemented a bash script that generates workloads with different numbers of jobs and run the schedulers on each of them. The final results are summarized in the graphs *preempt.pdf*, *load.pdf* and *time.pdf* showing comparisons on resources variation, cluster load and execution time respectively.  

The following command can be used to run the test and generate the graphs:

```
#!bash
test/test.sh
```

The following are two examples of *load.pdf* and *preempt.pdf* graphs:

![GREE­‐LB vs GREE and GREE-­L](./documentation/img/gllb.png "GREE­‐LB vs GREE and GREE-­L")

## DEPENDENCIES ###

Currently simdop has been tested on Linux and Mac OS X. In order to use simdop the following packages need to be installed:

* python (>= 2.7)
* python-pydot 
* python-matplotlib

If you experience problems with pydot (Couldn't import dot_parser) the following may solve the issue: [http://stackoverflow.com/questions/15951748/pydot-and-graphviz-error-couldnt-import-dot-parser-loading-of-dot-files-will](http://stackoverflow.com/questions/15951748/pydot-and-graphviz-error-couldnt-import-dot-parser-loading-of-dot-files-will)  

For the generation of the graphs in the "RUNNING TESTS" section the following packages are also required:

* R (r-base)
* r-cran-ggplot2


## WHO DO I TALK TO? ###

* matteo1.mazzucchelli[at]mail.polimi.it
* marco.rabozzi[at]mail.polimi.it