
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from task import Workload, Job, Task
from tree import Tree, Node, TypeNode
import os.path
import json
import datetime, time
import random, math
from scheduler.scheduler import Scheduler, SchedulerTypes

class LoadGeneration(object):

    def __init__(self, fileConfig = None):
        super(LoadGeneration, self).__init__()
        self.workload = {}
        self.config = {}
        self.fileConfig = fileConfig
        if not self.fileConfig == None:
            self.genLoad(self.fileConfig)


    def readConfiguration(self, fileConfig = None):
        if self.fileConfig == None and fileConfig == None:
            print "Error: Can't read file"
            return
        if not fileConfig == None:
            self.fileConfig = fileConfig
        if not os.path.isfile(self.fileConfig):
            print "Error: Can't read file"
            return
        with open(self.fileConfig) as jsonFile:
            self.config = json.load(jsonFile)


    def genLoad(self, fileConfig = None):
        self.readConfiguration(fileConfig)
        
        for workload in self.config:
            print "Generating workload: {}".format(workload)
            self.workload[workload] = self.genWorkload(workload,
                                                       self.config[workload])


    def genWorkload(self, idWorkload, loadConfig):
        jobCounter = 0

        maxLoad = loadConfig["bundle"] * loadConfig["end"] * loadConfig["load"]

        minT = loadConfig["tasks_per_job"]["min"]
        maxT = loadConfig["tasks_per_job"]["max"]

        rigidRatio = loadConfig["rigid_ratio"]
        minW = loadConfig["w"]["min"] * loadConfig["bundle"] * loadConfig["end"]
        maxW = loadConfig["w"]["max"] * loadConfig["bundle"] * loadConfig["end"]
        minH = loadConfig["h"]["min"] * loadConfig["bundle"]
        maxH = loadConfig["h"]["max"] * loadConfig["bundle"]

        wl = Workload(idWorkload)
        testWl = Workload(idWorkload)
        totalLoad = 0

        while totalLoad < maxLoad :
            idJob = "j" + str(jobCounter)
            job, loadTime = self.genJob(idJob, minT, maxT,
                                        rigidRatio, minW, maxW, minH, maxH)
            
            # calculate window 
            minTime = loadTime / loadConfig["bundle"]

            c = random.uniform(0, 1)
            y = int(c * c * (loadConfig["end"] - 2))
            start = random.randint(1, int(loadConfig["end"] - y))
            end = start + y

            job.setWindow(start, end)
            tree = self.genTree(job)
            job.setTree(tree)

            testWl.addJob(job)
            scheduler = Scheduler("GREEDY", testWl, loadConfig, False)
            scheduler.run()
            plan = scheduler.getPlan()

            if len(plan.getAcceptedJobs()):
                # add job to workload if can be scheduled
                wl.addJob(job)
                jobCounter += 1
                totalLoad += loadTime
            
            testWl.removeAllJobs()
            

        return wl


    def genJob(self, idJob, minT, maxT, rigidRatio, minW, maxW, minH, maxH):
        job = Job(idJob)
        maxTask = random.randint(minT, maxT)
        taskCounter = 0
        loadTime = 0

        while taskCounter < maxTask :
            idTask = job.getId() + "t" + str(taskCounter)
            task = self.genTask(idTask, rigidRatio, minW, maxW, minH, maxH)
            job.addTask(task.getId(), task)
            loadTime += task.getW()
            taskCounter += 1

        return job, loadTime


    def genTask(self, idTask, rigidRatio, minW, maxW, minH, maxH):
        rigid = (random.uniform(0, 1) <= rigidRatio)
        w = int(random.uniform(minW, maxW))
        h = int(random.uniform(minH, maxH))
        if rigid and h > w:
            h = w
        elif rigid:
            l = int(math.ceil(w / h))
            w = l * h
        t = Task(idTask, w, h, rigid)
        return t


    def genTree(self, job):
        nodes = job.getTasks().keys()

        typeNode = TypeNode.All
        if random.random() > .5:
            typeNode = TypeNode.Order

        root = self.addNode(nodes, typeNode)

        return Tree(root)


    def addNode(self, nodes, typeNode):
        if len(nodes) <= 1:
            return Node(TypeNode.Leaf, [], nodes[0])

        partition = random.randint(2, len(nodes))
        children = []


        start = 0
        numNodes = random.randint(1, len(nodes) - (partition - 1))
        end = start + numNodes

        for i in xrange(0, partition):

            nextType = TypeNode.All
            if typeNode == TypeNode.All:
                nextType = TypeNode.Order
            nextNodes = nodes[start:end]
            node = self.addNode(nextNodes, nextType)
            children.append(node)
            
            start = end
            if i == partition - 2:
                numNodes = len(nodes) - end + 1
            elif i < partition - 1:
                numNodes = random.randint(1, (len(nodes) - end) - (partition - i - 2))
            end = start + numNodes

        return Node(typeNode, children)


    def getLoad(self):
        return self.workload


    def writeLoad(self, fileName):
        fOut = open(fileName, "w")
        fOut.write(self.workload)
        fOut.close()
