
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from ..exception.planexception import PlanException
from ..default import floatPrecision

class Plan(object):
    def __init__(self, workload, maxBundle):
        super(Plan, self).__init__()

        # init workload
        self.jobs = workload.getWorkload()
        self.t2j = {}
        for job in self.jobs:
            tasks = job.getTasks()
            for taskId in tasks:
                self.t2j[taskId] = job.getId()


        # init commit state
        self.pendingCommit = False
        self.jobToCommit = None
        self.taskBundle = {}
        self.committedJobs = {}

        # init the plan
        self.maxTime = max([job.getEnd() for job in self.jobs])
        self.maxBundle = maxBundle
        self.plan = []
        self.freeBundle = []

        for i in xrange(0, self.maxTime):
            self.plan.append([]);
            self.freeBundle.append(self.maxBundle)


    def __str__(self):
        string = ''
        i = 0
        for t in self.plan:
            string = string + str(i) + str(' :: ') + str(t) + "\n"
            i = i + 1

        return string


    def initJobCommit(self, jobId):
        if self.pendingCommit:
            raise PlanException("Cannot start a new commit session, one session is already started")

        if jobId in self.committedJobs:
            raise PlanException("Job: " + str(jobId) + " already committed")            

        self.pendingCommit = True
        self.jobToCommit = jobId
        self.taskBundle = {}


    def addBundle(self, taskId, timeInstant, numOfBundle):

        if not self.pendingCommit:
            raise PlanException("Cannot append resources, a commit session must be started first with initJobCommit")

        if numOfBundle > 0:
            if taskId not in self.taskBundle:
                self.taskBundle[taskId] = {}
            if timeInstant in self.taskBundle[taskId]:
                self.taskBundle[taskId][timeInstant] = self.taskBundle[taskId][timeInstant] + numOfBundle
            else:
                self.taskBundle[taskId][timeInstant] = numOfBundle


    def computeFreeBundle(self, timeInstant):
        tempRes = 0
        for task in self.taskBundle: 
            if timeInstant in self.taskBundle[task]:
                tempRes = tempRes + self.taskBundle[task][timeInstant]

        return self.freeBundle[timeInstant] - tempRes


    def endCommit(self):

        if not self.pendingCommit:
            raise PlanException("Error: there is no job to commit ")

        jobId = self.jobToCommit

        if len(self.taskBundle) > 0:

            self.committedJobs[jobId] = {
                'start' : self.maxTime,
                'end' : 0,
                'tasks' : {}
            }

            for task in self.taskBundle:
                if len(self.taskBundle[task]) > 0:
                    taskStart = self.maxTime
                    taskEnd = 0
                    totResources = 0
                    minH = self.maxBundle
                    maxH = 0
                    totW = 0
                    l = 0

                    # assign bundle for task
                    for t in self.taskBundle[task]:
                        bundle = self.taskBundle[task][t]

                        if t > taskEnd:
                            taskEnd = t
                        if t < taskStart:
                            taskStart = t
                        if bundle > maxH:
                            maxH = bundle
                        if bundle < minH:
                            minH = bundle

                        totW = totW + bundle

                        self.plan[t].append((task, bundle))
                        self.freeBundle[t] = self.freeBundle[t] - bundle

                        if self.freeBundle[t] < -floatPrecision:
                            raise PlanException("Cannot commit task " + str(task) + " , not enough resources at time " + str(t))

                    # compute stats for the commited task
                    l = taskEnd - taskStart + 1

                    self.committedJobs[jobId]['end'] = max(self.committedJobs[jobId]['end'], taskEnd)
                    self.committedJobs[jobId]['start'] = min(self.committedJobs[jobId]['start'], taskStart)
                    self.committedJobs[jobId]['tasks'][task] = {
                        'l' : l,
                        'minH' : minH,
                        'maxH' : maxH,
                        'totW' : totW,
                        'start' : taskStart,
                        'end' : taskEnd
                    }

        # clear temp commit data
        self.resetCommit()


    def resetCommit(self):
        self.pendingCommit = False
        self.taskToCommit = None
        self.jobToCommit = None
        self.taskBundle = {}


    def getAcceptedJobs(self):
        return self.committedJobs

    def getPlan(self):
        return self.plan

    def getMaxTime(self):
        return self.maxTime
