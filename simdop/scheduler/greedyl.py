
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from ..tree import Tree, Node, TypeNode
import math


class GreedyL(object):
    def __init__(self):
        super(GreedyL, self).__init__()
        self.name = "greedy-L schedule"
        self.plan = None
        self.clusterConfig = None
        self.verbose = True

    def setPlan(self, plan):
        self.plan = plan

    def setVerbose(self, value):
        self.verbose = value


    def schedule(self, workload):

        print "Runnnig '{}'".format(self.name)

        # retrieve the list of jobs
        jobs = workload.getWorkload()
        for job in jobs:
            self.scheduleSingleJob(job)



    # private
    def scheduleSingleJob(self, job):

        self.plan.initJobCommit(job.getId())

        treeRoot = job.getTree().getRoot()
        self.computeNodeParameters(treeRoot, job.getTasks())
        result = self.scheduleNode(treeRoot,job.getStart(),job.getEnd(),job.getTasks())

        if result:
            self.plan.endCommit()
        else:
            self.plan.resetCommit()


        return result


    # private
    def scheduleNode(self, node, minT, maxT, tasks):
        
        # handles nodes of type All
        if node.getType() == TypeNode.All:
            minAllT = None

            for node in node.getChildren():
                result = self.scheduleNode(node, minT, maxT, tasks)

                if not result:
                    return False

            return True

        # handles nodes of type Order
        elif node.getType() == TypeNode.Order:

            totL = 0
            totW = 0

            children = node.getChildren()
            children.reverse()

            for node in children:
                totL = totL + node.getParam('l')
                totW = totW + math.sqrt(node.getParam('w'))

            availSmoothTime = maxT - minT - totL

            if(availSmoothTime < 0):
                return False

            end = maxT

            for node in children:

                if totW > 0:
                    begin = int(end - node.getParam('l') - math.floor(math.sqrt(node.getParam('w')) * availSmoothTime / totW))
                else:
                    begin = int(end - node.getParam('l'))

                result = self.scheduleNode(node, begin, end, tasks)

                if not result:
                    return False

                end = begin

            return True

        # handles leaf nodes
        elif node.getType() == TypeNode.Leaf:
            return self.scheduleTask(tasks[node.getId()], minT, maxT)



    def computeNodeParameters(self, node, tasks):
        
        # compute w and l parameters for each node
        nodeL = 0
        nodeW = 0

        # handles nodes of type All
        if node.getType() == TypeNode.All:

            nodesParams = []
            for child in node.getChildren():
                w, l = self.computeNodeParameters(child, tasks)
                nodesParams.append((w,l))

            maxL = max([x[1] for x in nodesParams])

            for nodeParam in nodesParams:
                w = nodeParam[0]
                l = nodeParam[1]
                
                if maxL == 0:
                    reductionFactor = 1
                else:
                    reductionFactor = 0.5 * (float(l) / maxL) + 0.5
                nodeW = nodeW + w*reductionFactor

            nodeL = maxL

        # handles nodes of type Order
        elif node.getType() == TypeNode.Order:

            for child in node.getChildren():
                w, l = self.computeNodeParameters(child, tasks)

                nodeW = nodeW + math.sqrt(w)
                nodeL = nodeL + l

            nodeW = nodeW * nodeW

        # handles leaf nodes
        elif node.getType() == TypeNode.Leaf:
            task = tasks[node.getId()]
            if(task.getRigid()):
                nodeL = task.getW() / task.getH()
            else:
                nodeW = task.getW()
                nodeL = math.ceil(task.getW() / task.getH())

        node.setParam('w', nodeW)
        node.setParam('l', nodeL)

        return nodeW, nodeL


    # private
    def scheduleRigidTask(self, task, minT, maxT):
        
        l = task.getW() / task.getH()

        tEnd = maxT
        while tEnd >= minT + l:

            # check that the task can end at tEnd - 1 and begin at tEnd - l
            canBePlaced = True

            for t in xrange(tEnd - l, tEnd):
                if self.plan.computeFreeBundle(t) < task.getH():
                    canBePlaced = False
                    break

            if canBePlaced:
                for t in xrange(tEnd - l, tEnd):
                    self.plan.addBundle(task.getId(), t, task.getH())

                return True

            tEnd = tEnd - 1

        return False


    # private
    def scheduleSmoothTask(self, task, minT, maxT):    

        t = maxT
        plannedLoad = 0


        if minT == maxT:
            return False

        loadT = task.getW() / float((maxT - minT)) 

        if loadT > task.getH():
            return False

        for t in xrange(minT, maxT):
            if self.plan.computeFreeBundle(t) < loadT:
                return False

            self.plan.addBundle(task.getId(), t, loadT)

        return True


    # private
    def scheduleTask(self, task, minT, maxT):
        
        if task.getRigid():
            return self.scheduleRigidTask(task, minT, maxT)
        
        return self.scheduleSmoothTask(task, minT, maxT)

