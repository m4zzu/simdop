
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from ..enum import enum
from greedy import Greedy
from greedyl import GreedyL
from greedylb import GreedyLB
from plan import Plan
import math

from ..exception.tasknotpresent import TaskNotPresent
from ..exception.loadnominimum import LoadNoMinimum
from ..exception.exceededresource import ExceededResource
from ..exception.timefail import TimeFail

from ..default import floatPrecision

from ..tree import TypeNode

SchedulerTypes = enum(
    GREEDY     = 0,
    GREEDY_L   = 1,
    GREEDY_LB  = 2
)


class Scheduler(object):
    infoScheduler = {
        SchedulerTypes.GREEDY      : Greedy(),
        SchedulerTypes.GREEDY_L    : GreedyL(),
        SchedulerTypes.GREEDY_LB   : GreedyLB(),
    }

    def __init__(self, sType = None, workload = None, clusterConfig = None, verbose = True):
        super(Scheduler, self).__init__()
        self.setType(sType)
        self.workload = workload
        self.clusterConfig = clusterConfig
        self.acceptedJobs = None
        self.acceptedTasks = None
        self.plan = None
        self.verbose = verbose

    def setType(self, sType):
        if sType == "GREEDY":
            self.type = SchedulerTypes.GREEDY
            return True
        elif sType == "GREEDY_L":
            self.type = SchedulerTypes.GREEDY_L
            return True
        elif sType == "GREEDY_LB":
            self.type = SchedulerTypes.GREEDY_LB
            return True

        print "Scheduler {} not found!".format(sType)
        return False

    def setWorkload(self, workload):
        self.workload = workload


    def getAcceptedTasks(self):
        return self.acceptedTasks


    def run(self):
        self.plan = Plan(self.workload, self.clusterConfig["bundle"])
        if self.type in self.infoScheduler:
            s = self.infoScheduler[self.type]

            s.setVerbose(self.verbose)
            s.setPlan(self.plan)
            s.schedule(self.workload)
            self.validatePlan(self.workload)

    def getPlan(self):
        return self.plan


    def validatePlan(self, workload):
        
        acceptedJobs = self.plan.getAcceptedJobs()
        wl = workload.getWorkload()
        for job in wl:
            if job.getId() in acceptedJobs:
                tasks = job.getTasks()
                startJob = acceptedJobs[job.getId()]["start"]
                endJob = acceptedJobs[job.getId()]["end"]
                acceptedTasks = acceptedJobs[job.getId()]["tasks"]
                for t, task in tasks.iteritems():

                    # check all tasks per jobs
                    if not t in acceptedTasks:
                        raise TaskNotPresent(t)

                    # check minimum workload for each task            
                    expected = task.getW()
                    accepted = acceptedTasks[t]["totW"]
                    if expected > accepted + floatPrecision:
                        print task.getId()
                        raise LoadNoMinimum(task, accepted)

                    # rigid task must have fixed h and l consecutive time stamp
                    # of resources allocation, and minH == maxH
                    tAccepted = acceptedTasks[t]
                    if task.isRigid():
                        if not tAccepted["maxH"] == tAccepted["minH"]:
                            raise TaskNotRigid(task, tAccepted)
                        if not tAccepted["maxH"] * tAccepted["l"] == tAccepted["totW"]:
                            raise TaskNotRigid(task, tAccepted)
                    else: # smooth
                        if tAccepted["maxH"] > task.getH() + floatPrecision:
                            raise ExceededResource(task, tAccepted, "h")

                    # check that all the task within a job start and end within
                    # the job window
                    if (tAccepted["start"] > tAccepted["end"]) or \
                       (startJob > tAccepted["start"]) or \
                       (endJob < tAccepted["end"]):
                            raise TimeFail(tAccepted, startJob, endJob)

                # check that task precedences within each job are respected
                if not self.checkPrecedences(job, acceptedTasks):
                    raise TimeFail(job, acceptedTasks)

        return True


    # private
    def checkPrecedences(self, job, tasks):
        check, maxTime = self.checkOrderTree(0, job.getTree().getRoot(), tasks)
        return check and job.getEnd() >= maxTime


    # private
    def checkOrderTree(self, minT, node, tasks):
        if node.getType() == TypeNode.Leaf:
            t = tasks[node.getId()]
            if minT <= t["start"]:
                return True, t["end"]
            return False, None
        elif node.getType() == TypeNode.All:
            children = node.getChildren()
            children.reverse()
            maxT = minT
            check = True
            for n in children:
                cTmp, maxTmp = self.checkOrderTree(minT, n, tasks)
                check = check and cTmp
                if not check:
                    return False, None
                maxT = max(maxT, maxTmp)
            return True, maxT
        elif node.getType() == TypeNode.Order:
            children = node.getChildren()
            children.reverse()
            for n in children:
                check, maxTmp = self.checkOrderTree(minT, n, tasks)
                if not check:
                    return False, None
                if not minT < maxTmp:
                    return False, None
                minT = maxTmp
            return True, maxTmp


    def computeMetrics(self, workload):

        acceptedJobs, acceptedTasks = self.getAcceptedJobsAndTasks(workload)

        load = self.computeAcceptedLoad() 
        # preempt = self.computePreemption(workload) / math.sqrt(load)
        preempt = float(self.computePreemption(workload) / float(load))


        acceptedJobs = list(acceptedJobs)
        acceptedJobs = [int(a.replace('j','')) for a in acceptedJobs]
        acceptedJobs.sort()

        print "Accepted jobs: {}".format(acceptedJobs)
        print "Preemption Ratio: {}".format(preempt)
        print "Load: {}".format(load)


    # private
    def getTaskToJobMap(self, workload):

        result = {}
        jobs = workload.getWorkload()
        for job in jobs:
            tasks = job.getTasks()

            for taskId in tasks:
                result[taskId] = job.getId()

        return result

    # private
    def getAcceptedJobsAndTasks(self, workload):

        if not self.acceptedJobs == None and not self.acceptedTasks == None:
            return self.acceptedJobs, self.acceptedTasks 

        t2j = self.getTaskToJobMap(workload)
        acceptedJobs = set()
        acceptedTasks = set()

        plan = self.plan.getPlan()
        maxT = len(plan)

        for t in xrange(0, maxT):
            resList = plan[t]
            for resData in resList:
                taskId = resData[0]
                acceptedJobs.add(t2j[taskId])
                acceptedTasks.add(taskId)

        self.acceptedTasks = acceptedTasks
        self.acceptedJobs = acceptedJobs

        return self.acceptedJobs, self.acceptedTasks

    # private
    def computePreemption(self, workload):

        plan = self.plan.getPlan()
        localPlan = [[]] + plan + [[]]
        acceptedJobs, acceptedTasks = self.getAcceptedJobsAndTasks(workload)
        maxT = len(localPlan)
        planPerTask = {}
        preemption = 0

        for task in acceptedTasks:
            planPerTask[task] = []
            for t in xrange(0, maxT):
                planPerTask[task].append(0)

        for t in xrange(0, maxT):
            for resData in localPlan[t]:
                planPerTask[resData[0]][t] = resData[1]

        for task in acceptedTasks:
            for t in xrange(1, maxT):
                preemption = preemption \
                           + abs(planPerTask[task][t] - planPerTask[task][t - 1])


        return preemption


    # private
    def computeAcceptedLoad(self):
        
        jobs = self.plan.getAcceptedJobs()
        load = 0
        for jId in jobs:
            for tId, task in jobs[jId]["tasks"].iteritems():
                load += task["totW"]
        return load
