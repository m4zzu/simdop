
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from ..tree import Tree, Node, TypeNode

class Greedy(object):
    def __init__(self):
        super(Greedy, self).__init__()
        self.name = "greedy schedule"
        self.plan = None
        self.clusterConfig = None
        self.verbose = True

    def setPlan(self, plan):
        self.plan = plan

    def setVerbose(self, value):
        self.verbose = value


    def schedule(self, workload):

        if self.verbose:
            print "Runnnig '{}'".format(self.name)

        # retrieve the list of jobs
        jobs = workload.getWorkload()
        for job in jobs:
            self.scheduleSingleJob(job)



    # private
    def scheduleSingleJob(self, job):

        self.plan.initJobCommit(job.getId())

        treeRoot = job.getTree().getRoot()
        result, startT = self.scheduleNode(
            treeRoot,
            job.getStart(),
            job.getEnd(),
            job.getTasks()
        )

        if result:
            self.plan.endCommit()
        else:
            self.plan.resetCommit()

        return result


    # private
    def scheduleNode(self, node, minT, maxT, tasks):
        
        # handles nodes of type All
        if node.getType() == TypeNode.All:
            minAllT = None

            for node in node.getChildren():
                result, beginT = self.scheduleNode(node, minT, maxT, tasks)

                if not result:
                    return (False, None)

                if minAllT == None:
                    minAllT = beginT
                else:
                    minAllT = min(minAllT, beginT)

            return (True, minAllT)

        # handles nodes of type Order
        elif node.getType() == TypeNode.Order:
            children = node.getChildren()
            children.reverse()
            for node in children:
                result, beginT = self.scheduleNode(node, minT, maxT, tasks)

                if not result:
                    return (False, None)

                maxT = beginT

            return (True, beginT)

        # handles leaf nodes
        elif node.getType() == TypeNode.Leaf:
            return self.scheduleTask(tasks[node.getId()], minT, maxT)


    # private
    def scheduleRigidTask(self, task, minT, maxT):
        

        l = task.getW() / task.getH()

        tEnd = maxT
        while tEnd >= minT + l:

            # check that the task can end at tEnd - 1 and begin at tEnd - l
            canBePlaced = True

            for t in xrange(tEnd - l, tEnd):
                if self.plan.computeFreeBundle(t) < task.getH():
                    canBePlaced = False
                    break

            if canBePlaced:
                for t in xrange(tEnd - l, tEnd):
                    self.plan.addBundle(task.getId(), t, task.getH())

                return (True, tEnd - l)

            tEnd = tEnd - 1

        return (False, None)


    # private
    def scheduleSmoothTask(self, task, minT, maxT):

        t = maxT - 1
        plannedLoad = 0

        while t >= minT and plannedLoad < task.getW():

            loadT = min([
                         task.getH(),
                         self.plan.computeFreeBundle(t),
                         task.getW() - plannedLoad
                        ])
            if loadT > 0:
                self.plan.addBundle(task.getId(), t, loadT)
                plannedLoad = plannedLoad + loadT

            t = t - 1

        if plannedLoad < task.getW():
            return False, None

        return True, t

    # private
    def scheduleTask(self, task, minT, maxT):
        
        if task.getRigid():
            return self.scheduleRigidTask(task, minT, maxT)
        
        return self.scheduleSmoothTask(task, minT, maxT)
