
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from ..tree import Tree, Node, TypeNode
import math
import random
from ..default import floatPrecision



class GreedyLB(object):
    def __init__(self):
        super(GreedyLB, self).__init__()
        self.name = "greedy-LB schedule"
        self.plan = None
        self.clusterConfig = None
        self.enableRigidTaskSlack = True
        self.verbose = True

    def setPlan(self, plan):
        self.plan = plan

    def setVerbose(self, value):
        self.verbose = value


    def schedule(self, workload):

        print "Runnnig '{}'".format(self.name)

        # retrieve the list of jobs
        jobs = workload.getWorkload()
        for job in jobs:
            self.scheduleSingleJob(job)

    # private
    def initPlan(self, maxTime):
        self.plan = []
        for i in xrange(0, maxTime):
            self.plan.append([]);


    # private
    def scheduleSingleJob(self, job):

        self.plan.initJobCommit(job.getId())

        treeRoot = job.getTree().getRoot()
        self.computeNodeParameters(treeRoot, job.getTasks())
        result, taskBegin = self.scheduleNode(treeRoot,job.getStart(),job.getEnd(),job.getTasks())

        if result:
            self.plan.endCommit()
        else:
            self.plan.resetCommit()

        return result


    # private
    def scheduleNode(self, node, minT, maxT, tasks):
        
        # handles nodes of type All
        if node.getType() == TypeNode.All:
            minAllT = maxT

            for node in node.getChildren():
                result, taskBegin = self.scheduleNode(node, minT, maxT, tasks)

                if not result:
                    return False, None

                minAllT = min(minAllT, taskBegin)

            return True, minAllT

        # handles nodes of type Order
        elif node.getType() == TypeNode.Order:

            totL = 0
            totW = 0

            children = node.getChildren()
            children.reverse()

            for node in children:
                totL = totL + node.getParam('l')
                totW = totW + math.sqrt(node.getParam('w'))

            availSmoothTime = maxT - minT - totL

            if(availSmoothTime < 0):
                return False, None

            end = maxT

            for node in children:

                if totW > 0:
                    currentSmoothtTime = math.floor(math.sqrt(node.getParam('w')) * availSmoothTime / totW)
                    begin = int(end - node.getParam('l') - currentSmoothtTime)
                    availSmoothTime = availSmoothTime - currentSmoothtTime
                    totW = totW - math.sqrt(node.getParam('w'))
                else:
                    begin = int(end - node.getParam('l'))


                if node.getParam('w') == 0 and self.enableRigidTaskSlack:
                    result, taskBegin = self.scheduleNode(node, minT, end, tasks)
                else:
                    result, taskBegin = self.scheduleNode(node, begin, end, tasks)

                if not result:
                    return False, None

                delta = begin - taskBegin
                if delta > 0:
                    begin = taskBegin
                    availSmoothTime = availSmoothTime - delta
            

                end = begin

            return True, minT

        # handles leaf nodes
        elif node.getType() == TypeNode.Leaf:
            return self.scheduleTask(tasks[node.getId()], minT, maxT)



    def computeNodeParameters(self, node, tasks):
        
        # compute w and l parameters for each node
        nodeL = 0
        nodeW = 0

        # handles nodes of type All
        if node.getType() == TypeNode.All:

            nodesParams = []
            for child in node.getChildren():
                w, l = self.computeNodeParameters(child, tasks)
                nodesParams.append((w,l))

            maxL = max([x[1] for x in nodesParams])

            for nodeParam in nodesParams:
                w = nodeParam[0]
                l = nodeParam[1]
                
                if maxL == 0:
                    reductionFactor = 1
                else:
                    reductionFactor = 0.5 * (float(l) / maxL) + 0.5
                nodeW = nodeW + w*reductionFactor

            nodeL = maxL

        # handles nodes of type Order
        elif node.getType() == TypeNode.Order:

            for child in node.getChildren():
                w, l = self.computeNodeParameters(child, tasks)

                nodeW = nodeW + math.sqrt(w)
                nodeL = nodeL + l

            nodeW = nodeW * nodeW

        # handles leaf nodes
        elif node.getType() == TypeNode.Leaf:
            task = tasks[node.getId()]
            if(task.getRigid()):
                nodeL = task.getW() / task.getH()
                nodeW = 0
            else:
                nodeW = task.getW()
                nodeL = math.ceil(task.getW() / task.getH())

        node.setParam('w', nodeW)
        node.setParam('l', nodeL)

        return nodeW, nodeL


    # private
    def scheduleRigidTask(self, task, minT, maxT):
        
        l = task.getW() / task.getH()

        tEnd = maxT
        while tEnd >= minT + l:

            # check that the task can end at tEnd - 1 and begin at tEnd - l
            canBePlaced = True

            for t in xrange(tEnd - l, tEnd):
                if self.plan.computeFreeBundle(t) < task.getH():
                    canBePlaced = False
                    break

            if canBePlaced:
                for t in xrange(tEnd - l, tEnd):
                    self.plan.addBundle(task.getId(), t, task.getH())

                return True, tEnd - l

            tEnd = tEnd - 1
        
        return False, None


    # private
    def scheduleSmoothTask(self, task, minT, maxT):
        
        resourcesToPlace = task.getW()

        firstPass = {}

        if minT == maxT:
            return False, None

        # schedule resources
        if resourcesToPlace > 0:
            for t in xrange(minT, maxT):
                load = float(resourcesToPlace) / (maxT - t)
                avail = self.plan.computeFreeBundle(t) 

                if load > task.getH():
                    return False, None

                if load > avail:
                    load = avail

                firstPass[t] = load

                if load > 0:
                    self.plan.addBundle(task.getId(), t, load)

                resourcesToPlace = resourcesToPlace - load

        # try to reschedule missing resources                
        if resourcesToPlace > 0:

            for t in xrange(maxT - 1, minT - 1, -1):
                load = float(resourcesToPlace) / (t - minT + 1)
                
                avail = self.plan.computeFreeBundle(t) 

                if load + firstPass[t] > task.getH():
                    load = task.getH() - firstPass[t]

                if load > avail:
                    load = avail

                if load > 0:
                    self.plan.addBundle(task.getId(), t, load)

                resourcesToPlace = resourcesToPlace - load
        
        if resourcesToPlace > floatPrecision:
            return False, None

        return True, minT


    # private
    def scheduleTask(self, task, minT, maxT):
        
        if task.getRigid():
            return self.scheduleRigidTask(task, minT, maxT)
        
        return self.scheduleSmoothTask(task, minT, maxT)

