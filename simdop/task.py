
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from tree import Tree

"""
This class defines a Workload:
    name    the name of the workload
    jobs    a list that contains all the jobs
"""
class Workload(object):

    def __init__(self, name):
        super(Workload, self).__init__()
        self.name = name
        self.jobs = []

    def __str__(self):
        st = "WORKLOAD: {}\n\n".format(self.name)
        for job in self.jobs:
            st += str(job) + "\n"
        return st

    def addJob(self, job):
        self.jobs.append(job)

    def removeAllJobs(self):
        self.jobs = []

    def getName(self):
        return self.name

    def getWorkload(self):
        return self.jobs


"""
This class defines a Job:
    id      the name of the job
    start   the time of the window start
    end     the time of the window end
    tasks   a map that contains all the tasks in the job
    edges   a list that describes the dependencies between tasks
"""
class Job(object):

    def __init__(self, id, start = None, end = None, tasks = None,
                           rdl = None, tree = None):
        super(Job, self).__init__()
        self.id = id
        self.start = start
        self.end = end
        if tasks == None:
            self.tasks = {}
        else:
            self.tasks = tasks
        if rdl == None and tree == None:
            self.rdl = None
            self.tree = Tree()
        elif rdl == None:
            self.tree = tree
            self.rdl = tree.getRDL()
        elif tree == None:
            self.rdl = rdl
            self.tree = Tree(rdl = rdl)

    def __str__(self):
        st = "job: {}\n".format(self.id)
        st += "window: {} {}\n".format(self.start, self.end)
        st += "tasks:\n"
        for task in self.tasks.itervalues():
            st += "\t" + str(task)
        # st += "edges:\n"
        # for edge in self.edges:
        #     st += "\t{} ~> {}\n".format(edge[0], edge[1])
        st += "rdl: {}".format(self.rdl)
        return st

    def addTask(self, taskId, task):
        self.tasks[taskId] = task

    def setWindow(self, start, end):
        self.setStart(start)
        self.setEnd(end)

    def setStart(self, start):
        self.start = start

    def setEnd(self, end):
        self.end = end

    def setTasks(self, tasks):
        self.tasks = tasks

    def setTree(self, tree):
        self.tree = tree
        self.rdl = tree.getRDL()

    def setRDL(self, rdl):
        self.rdl = rdl
        self.tree.setRDL(rdl)

    def getId(self):
        return self.id

    def getStart(self):
        return self.start

    def getEnd(self):
        return self.end

    def getTasks(self):
        return self.tasks

    def getTree(self):
        return self.tree

    def getRDL(self):
        return self.rdl


"""
This class defines a Task
    id      the id of the task
    w       the area that will occupied the task
    h       the height of the task (number of bundle)
    rigid   it defines the type of the task (rigid or not rigid)
"""
class Task(object):

    def __init__(self, id, w = None, h = None, rigid = None):
        super(Task, self).__init__()
        self.id = id
        self.w = w
        self.h = h
        self.rigid = rigid

    def __str__(self):
        return "Task ( id: {}, w: {}, h: {}, rigid: {} )\n".format(self.id,
                                                                   self.w,
                                                                   self.h,
                                                                   self.rigid)

    def getId(self):
        return self.id

    def getW(self):
        return self.w

    def getH(self):
        return self.h

    def getRigid(self):
        return self.rigid

    def isRigid(self):
        return self.rigid

    def setW(self, w):
        self.w = w

    def setH(self, h):
        self.h = h

    def setRigid(self, rigid):
        self.rigid = rigid
