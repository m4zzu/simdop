
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from enum import enum

class Tree(object):
    def __init__(self, root = None, rdl = None):
        super(Tree, self).__init__()
        self.root = root
        self.rdl = rdl

    def setRoot(self, node):
        self.root = node
        self.rdl = None

    def setRDL(self, rdl):
        self.rdl = rdl
        self.root = None

    def getRoot(self):
        if self.root == None and not self.rdl == None:
            self.root = self.readRDL(self.rdl)
        return self.root

    # private
    def readRDL(self, rdl):
        node = None
        if (rdl[0] == "A" or rdl[0] == "a") and rdl[1] == "(":
            node = Node(TypeNode.All, [], "")
            node.setChildren(self.readRDLsub(rdl[2:]))
        if (rdl[0] == "O" or rdl[0] == "o") and rdl[1] == "(":
            node = Node(TypeNode.Order, [], "")
            node.setChildren(self.readRDLsub(rdl[2:]))
        return node

    # private
    def readRDLsub(self, rdl):
        nodes = []
        i = 0
        while not rdl[i] == ")":
            if rdl[i] == "A" or rdl[i] == "a" or rdl[i] == "O" or rdl[i] == "o":
                nodes.append(self.readRDL(rdl[i:]))
                # skip loop
                countPar = 1
                i += 2
                while not countPar == 0:
                    if rdl[i] == "(":
                        countPar += 1
                    elif rdl[i] == ")":
                        countPar -= 1
                    i += 1
            else:
                idNode = ""
                while not rdl[i] == "," and not rdl[i] == ")":
                    idNode += rdl[i]
                    i += 1
                if not idNode == "":
                    nodes.append(Node(TypeNode.Leaf, [], idNode))
                else:
                    i += 1
        return nodes


    def getRDL(self):
        if self.rdl == None and not self.root == None:
            self.rdl = self.readTree(self.root)
        return self.rdl

    # private
    def readTree(self, node):
        typeNode = node.getType()
        if typeNode == TypeNode.Nope:
            return "!!!"
        elif typeNode == TypeNode.All:
            ret = "A("
            nodes = node.getChildren()
            for i in xrange(0, len(nodes)):
                ret += self.readTree(nodes[i])
                if i < len(nodes) - 1:
                    ret += ","
            return ret + ")"
        elif typeNode == TypeNode.Order:
            ret = "O("
            nodes = node.getChildren()
            for i in xrange(0, len(nodes)):
                ret += self.readTree(nodes[i])
                if i < len(nodes) - 1:
                    ret += ","
            return ret + ")"

        elif typeNode == TypeNode.Leaf:
            return node.getId()

    def getGraph(self):
        pass


TypeNode = enum(Nope  = 0,
                All   = 1,
                Order = 2,
                Leaf  = 3)


class Node(object):
    def __init__(self, type = TypeNode.Nope, children = [], id = ""):
        super(Node, self).__init__()
        self.type = type
        self.children = children
        self.id = id
        self.params = {}

    def __str__(self):
        return "N({}, {}, {})".format(self.type, self.children, self.id)
    
    def setType(self, type):
        self.type = type

    def setChildren(self, children):
        self.children = children

    def addChild(self, child):
        self.children.append(child)

    def getId(self):
        return self.id

    def getParam(self, name):
        if name in self.params:
            return self.params[name]
        return None

    def setParam(self, name, value):
        self.params[name] = value
        
    def getType(self):
        return self.type

    def getChildren(self):
        return self.children

    def isLeaf(self):
        return self.type == TypeNode.leaf

