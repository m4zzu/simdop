
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os.path
import json

class Configuration(object):
    
    def __init__(self, fileName = None):
        super(Configuration, self).__init__()
        self.configuration = {}
        self.fileName = fileName
        if not self.fileName == None:
            self.read(self.fileName)

    """ Read method"""
    def read(self, fileName = None):
        if self.fileName == None and fileName == None:
            print "Error: Can't read file"
            return

        if not fileName == None:
            self.fileName = fileName

        if not os.path.isfile(self.fileName):
            print "Error: Can't read file"
            return

        with open(self.fileName) as jsonFile:
            self.configuration = json.load(jsonFile)
        
    """ set method"""
    def set(self, key, value):
        self.configuration[key] = value

    """ get method"""
    def get(self, key):
        if key in self.configuration:
            return self.configuration[key]
        return None

    """ get fileName"""
    def getFileName(self):
        return self.fileName

    """ get configuration as dictionary """
    def getConfiguration(self):
        return self.configuration

    """ get configuration as json """
    def getConfigurationJson(self):
        return json.dumps(self.configuration, ensure_ascii = False)
