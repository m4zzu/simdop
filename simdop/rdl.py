
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from task import Workload, Job, Task
import os.path
import pydot

class RDL(object):
    
    def __init__(self, rdl = None):
        super(RDL, self).__init__()
        self.rdl = rdl
        if not self.rdl == None:
            self.workload = Workload(self.rdl)
            self.parse(self.rdl)

    def parse(self, rdl = None):
        if self.rdl == None and rdl == None:
            print "Error: Can't read file"
            return

        if not rdl == None:
            self.rdl = rdl

        if not os.path.isfile(self.rdl):
            print "Error: Can't read file"
            return

        self.workload = Workload(self.rdl)
        job = None
        with open(self.rdl) as jsonFile:
            for line in jsonFile.readlines():
                line = line.strip()
                
                """ blank line or comment """
                if len(line) <= 0 or line[0] == "#":
                    pass

                line = line.split(' ')
                token = line[0]
                if token == "job":
                    if not job == None and self.checkJob(job):
                        self.workload.addJob(job) 
                    job = self.parseJob(line[1:])
                elif token == "window":
                    job = self.parseWindow(line[1:], job)
                elif token == "task":
                    job = self.parseTask(line[1:], job)
                elif token == "rdl":
                    job = self.parseRDL(line[1:], job)

        if not job == None and self.checkJob(job):
            self.workload.addJob(job)


    def parseJob(self, args):
        job = None
        if len(args) >= 1:
            jobId = args[0]
            job = Job(jobId)
        return job

    def parseWindow(self, args, job):
        if not job == None and len(args) >= 2:
            start = int(args[0])
            end = int(args[1])
            job.setWindow(start, end)
        return job

    def parseTask(self, args, job):
        if not job == None and len(args) >= 4:
            taskId = args[0]
            w = int(args[1])
            h = int(args[2])
            if(args[3].lower() == 'true'):
                rigid = True
            else:
                rigid = False
            task = Task(taskId, w, h, rigid)
            job.addTask(taskId, task)
        return job

    def parseRDL(self, args, job):
        if not job == None and len(args) >= 1:
            job.setRDL(args[0])
        return job


    def checkJob(self, job):
        rdl = job.getRDL()
        for ch in ["A", "O", "(", ")", "a", "o", ","]:
            if ch in rdl:
                rdl = rdl.replace(ch, " ")
        rdl = rdl.split()
        for tId in job.getTasks().keys():
            if not tId in rdl:
                print "RDL error: deleted job {}, cause no task {} in rdl".format(job.getId(), tId)
                return False
        return True


    def getWorkload(self):
        return self.workload

    def setWorkload(self, workload):
        self.workload = workload
        
    def writeGraph(self, fileName = None):
        if fileName == None:
            fileName = "graph.pdf"

        graph = pydot.Dot(graph_type = 'digraph')
        for job in self.workload.getWorkload():
            for task in job.getTasks():
                node = pydot.Node(task)
                graph.add_node(node)
            for ed in job.getEdges():
                edge = pydot.Edge(ed[0], ed[1])
                graph.add_edge(edge)

        graph.write_pdf(fileName)
