
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

__fileConfig = "config/config.json"
__fileRDL = "test/workload.rdl"
__fileGraph = "graph.pdf"

__scheduler = False
__schedType = "greedy"

__generation = False
__graph = False


floatPrecision = 0.000000001
