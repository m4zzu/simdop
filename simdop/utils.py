
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from configuration import Configuration
from generation import LoadGeneration
from task import Tree
from rdl import RDL
from scheduler.scheduler import Scheduler, SchedulerTypes
from time import time
import matplotlib.pyplot as plt
from random import randint, seed


def generationTask(fileConfig, fileRDL, graph, fileGraph):
    load = LoadGeneration(fileConfig)
    rdls = {}

    fout = open(fileRDL, "w")
    for rdl, wl in load.getLoad().iteritems():
        rdls[rdl] = RDL()
        rdls[rdl].setWorkload(wl)
        fout.write("# {}\n".format(wl.getName()))
        for j in wl.getWorkload():
            fout.write("job {}\n".format(j.getId()))
            fout.write("\twindow {} {}\n".format(j.getStart(), j.getEnd()))
            for tId, t in j.getTasks().iteritems():
                fout.write("\ttask {} {} {} {}\n".format(tId, t.getW(), t.getH(),
                                                         t.getRigid()))
            fout.write("\trdl {}\n".format(j.getRDL()))

    fout.close()


def schedulerTask(fileConfig, schedType, fileRDL, graph, fileGraph):
    print "Scheduler: {}".format(schedType)
    config = Configuration(fileConfig)
    rdl = RDL(fileRDL)

    wl = rdl.getWorkload()

    scheduler = Scheduler(schedType, wl, config.get('cluster'))
    start = time()
    scheduler.run()
    print "Time: {}".format(time() - start)

    scheduler.computeMetrics(wl)

    if graph:
        plan = scheduler.getPlan().getPlan()
        makePlanGraph(plan, fileGraph, schedType, scheduler.getAcceptedTasks(), config.get('cluster')['bundle'])


def makePlanGraph(plan, fileGraph, schedType, acceptedTasks, bundle):
    print "Generating graph..."
    fileGraph = fileGraph.split(".")
    fileGraph = "{}_{}.pdf".format(fileGraph[0], schedType)
    
    index = [i for i in xrange(0, len(plan))]
    data = {}
    acceptedTasks = list(acceptedTasks)
    acceptedTasks.sort()
    for tId in acceptedTasks:
        data[tId] = [0] * len(plan)

    for i in xrange(0, len(plan)):
        tot = 0
        items = plan[i]
        items.sort()
        for item in items:
            data[item[0]][i] = item[1] + tot
            tot += item[1]

    seed(10)
    r = lambda: randint(0, 255)
    color = {}
    for tId in acceptedTasks:
        color[tId] = "#%02X%02X%02X" % (r(), r(), r())

    old = [0] * len(plan)
    acceptedTasks.reverse()
    for tId in acceptedTasks:
        plt.bar(index, data[tId], color = color[tId], label = tId)
    
    art = [plt.legend(loc = 9, bbox_to_anchor = (.0, -.22, 1, .1), ncol = 12,
                      prop = {'size': 6}, borderaxespad = 0., mode = "expand")]

    plt.title("Plan")
    plt.xlabel("time [s]")
    plt.ylabel("resource [bundle]")
    plt.ylim([0, bundle])
    plt.savefig(fileGraph, additional_artists = art, bbox_inches = "tight")
    plt.close()

def testTask(fileConfig, fileInput):
    print "Test!\n"
    config = Configuration(fileConfig)
    rdl = RDL(fileInput)
    wl = rdl.getWorkload()
    for job in wl.getWorkload():
        print "RDL from file:", job.getRDL()
        tr = Tree(job.getTree().getRoot())
        print "RDL from tree:", tr.getRDL()
        print

