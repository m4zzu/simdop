# scheduler.mod
#
# The following is a simple MILP model for the reservation problem on
# big-data systems written using the GNU MathProg language.
#
# The model supports the definition of a set of tasks belonging to a specific job.
# All the tasks of a job must be executed within the job window
# and precedence relations can be specified among tasks.
#
# The types of supported tasks are:
# - rigid: fixed resources allocation (h) for a contiguous amount of time (w/h)
# - malleable: maximum instantaneous resources allocation (h), fixed total amount of workload (w)
#
# The objective function consider a linear combination of two metrics:
# - accepted load: the utilization of the system over the considered overall time interval 
# - resources variation: the sum of variations of resources scaled by h for each task
#
# See example.dat for an example of a simple input workload
#
# In order to translate the model to a standard lp file format, glpk can be used as follow:
#
# glpsol -m scheduler.mod -d example.dat --check --wlp PROBLEM_FILE.lp
#
# Then, to search for a solution with Gurobi use:
#
# gurobi_cl ResultFile=solution.out PROBLEM_FILE.lp
#

# SETS


# TODO: acceptance weighted on job size

# set of tasks
set T;

# set of jobs
set J;

# set of tasks precedences
set P dimen 2;




# PARAMS

# the weight to assign to accepted load metrics
param acceptedLoadWeight;

# the weight to assign to resources variation metrics
param resourcesVariationWeight;

# avaialble bundles
param bundles;

# length of the time interval to consider
param maxTime;

# window start of a job
param window_start{J};

# window end of a job
param window_end{J};

# task to job mapping
param t2j{T};

# task workload
param w{T};

# max parallelism for a task
param h{T};

# whether a task is rigid or not (malleable)
param rigid{T};

# set of timestemps
set I := 0 .. (maxTime - 1);



# VARIABLES

# number of resources used by task t at time i
var x{T,I} >= 0;

# whether rigid task t is active at time i
var xr{t in T, i in I: rigid[t] == 1} binary;

# variable set to 1 from the first instant in which the task is active until the end of time (a = after)
var a{t in T, i in I} binary;

# variable set to 1 from time 0 until the task is completed (b = before)
var b{t in T, i in I} binary;

# absolute value of x discrete derivative over time
var y{t in T, i in I};

# binary variable set to 1 iff job j is accepted
var accept{J} binary;

# measures the resources variation on the system
var resourcesVariation >= 0;

# measures the accepted load ratio
var acceptedLoad >= 0;


# OBJECTIVE

maximize gain:
	acceptedLoadWeight * acceptedLoad - resourcesVariationWeight * resourcesVariation;

# CONSTRAINTS

subject to maxParallelism{t in T, i in I}:
	x[t,i] <= h[t];

subject to maxResources{i in I}:
	sum{t in T} x[t,i] <= bundles;

subject to rigidResourceRequirement{t in T, i in I: rigid[t] == 1}:
	x[t,i] = xr[t,i]*h[t];

subject to rigidTimeRequirement{t in T: rigid[t] == 1}:
	sum{i in I} y[t,i] = 2*h[t]*accept[t2j[t]]; # they used: <= 2*h[t] 

subject to beforeWindowConstraint{t in T, i in I: i < window_start[t2j[t]]}:
	x[t,i] = 0;

subject to afterWindowConstraint{t in T, i in I: i >= window_end[t2j[t]]}:
	x[t,i] = 0;

subject to loadConstraint{t in T}:
	sum{i in I} x[t,i] = w[t]*accept[t2j[t]];

subject to precedenceConstraint{t1 in T, t2 in T, i in I: (t1,t2) in P}:
	b[t1,i] + a[t2,i] <= 1;



# SEMANTICS CONSTRAINTS

subject to aSemantics{t in T, i in I}:
	a[t,i] >= x[t,i] / h[t];
subject to aSemantics2{t in T, i in I: i > 0}:
	a[t,i] >= a[t,i-1];

subject to bSemantics{t in T, i in I}:
	b[t,i] >= x[t,i] / h[t];
subject to bSemantics2{t in T, i in I: i < maxTime - 1}:
	b[t,i] >= b[t,i+1];

subject to ySemantics{t in T, i in I: i > 0}:
	y[t,i] >= x[t,i] - x[t,i-1];
subject to ySemantics2{t in T, i in I: i > 0}:
	y[t,i] >= x[t,i-1] - x[t,i];
subject to ySemantics3{t in T}:
	y[t,0] = x[t,0];

subject to acceptedLoadSemantics:
	acceptedLoad = ( sum{j in J} (accept[j] * ( sum{t in T: t2j[t] == j} w[t] ))) / (bundles * maxTime);

subject to resourcesVariationSemantics:
	resourcesVariation = sum{t in T, i in I} y[t,i] / h[t];

# CUTS

# this cut can be used only if the x variables are integer
# subject to minPreemptionForFloatingTasks{t in T: rigid[t] == 0}:
#	sum{i in I} y[t,i] >= 2*accept[t2j[t]];

subject to abxrRelation{t in T, i in I: rigid[t] == 1}:
	xr[t,i] = a[t,i] + b[t,i] - 1;

end;

